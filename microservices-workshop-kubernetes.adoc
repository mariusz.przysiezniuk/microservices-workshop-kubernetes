= Workshop Kubernetes
Arkadiusz Tułodziecki <atulodzi@gmail.com>
1.0, Oct 4, 2022: Microservices development workshops
:toc: left
:toclevels: 2
:source-highlighter: rouge
:encoding: utf-8
:icons: font
:url-quickref: https://docs.asciidoctor.org/asciidoc/latest/syntax-quick-reference/

== Kubernetes preparation

=== Install and configure prerequisites

[TIP]
Latest version of required steps is available here: https://kubernetes.io/docs/setup/production-environment/container-runtimes/

First we need to be sure that required modules and network configuration is set:

[source,bash]
----
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
----

After setting this up for the future we need to load those modules immediately

[source,bash]
----
sudo modprobe overlay
sudo modprobe br_netfilter
----

Now we need to set up system level configuration

[source,bash]
----
# sysctl params required by setup, params persist across reboots
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
----

and after this to apply it immediately run this command:

[source,bash]
----
# Apply sysctl params without reboot
sudo sysctl --system
----

Disable swap on all nodes

[source,bash]
----
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
----

[WARNING]
Check `/etc/fstab` after this to confirm that swap is no longer in use and check with `free -m`

=== Container runtimes

==== Option 1: docker

[TIP]
Latest version of installation procedure can be found here: https://docs.docker.com/engine/install/ubuntu/

Install the required packages for Docker.

[source,bash]
----
sudo apt-get update -y
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
----

Add the Docker GPG key and apt repository.

[source,bash]
----
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
----

Install the Docker community edition.

[source,bash]
----
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
----

Add the docker daemon configurations to use systemd as the cgroup driver.

[source,bash]
----
cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
----

Start and enable the docker service.

[source,bash]
----
sudo systemctl enable docker
sudo systemctl daemon-reload
sudo systemctl restart docker
----

You need to edit `/etc/containerd/config.toml` file and be sure that `cni` is not disabled. If it is - remove it from
disabled plugins and restart containerd `sudo systemctl restart containerd`

==== Option 2: containerd

This one works only on special prepared version of Ubuntu

[source,bash]
----
sudo apt-get update -y
sudo apt-get install containerd -y
----

Now configure containerd

[source,bash]
----
sudo mkdir -p /etc/containerd
----

generate contents of the containerd configuration

[source,bash]
----
sudo containerd config default | sudo tee /etc/containerd/config.toml
----

restart containerd

[source,bash]
----
sudo systemctl restart containerd
----


=== Install Kubeadm & Kubelet & Kubectl on all Nodes

[TIP]
Latest version of this instruction can be found here: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/

Install the required dependencies.

[source,bash]
----
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
----

Add the GPG key and apt repository.

[source,bash]
----
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
----

Update apt and install kubelet, kubeadm, and kubectl.

[source,bash]
----
sudo apt-get update -y
sudo apt-get install -y kubelet kubeadm kubectl
----

Add hold to the packages to prevent upgrades.

[source,bash]
----
sudo apt-mark hold kubelet kubeadm kubectl
----

=== Prepare kubeadm configuration file to set up Control Plane

We will be using yaml configuration files to init our cluster with the kubeadm.

==== Option 1: Flannel kubeadm configuration

We need to create `kube-config.yml` file looking like this for flannel type of network:

[source,yaml]
----
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
networking:
  dnsDomain: cluster.local
  podSubnet: 10.244.0.0/16
  serviceSubnet: 10.96.0.0/12
apiServer:
  extraArgs:
    service-node-port-range: 8000-31274
---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
serverTLSBootstrap: true
----

==== Option 2: Calico kubeadm configuration

We need to create `kube-config.yml` file looking like this for Calico type of network:

[source,yaml]
----
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
networking:
  dnsDomain: cluster.local
  podSubnet: 192.168.0.0/16
  serviceSubnet: 10.96.0.0/12
apiServer:
  extraArgs:
    service-node-port-range: 8000-31274
----

[TIP]
More detailed calico version of this instruction can be found here: https://devopscube.com/setup-kubernetes-cluster-kubeadm/


=== Initialize Kubeadm On Master Node To set up Control Plane

Then we need to run kubeadm:

[source,bash]
----
sudo kubeadm init --config kube-config.yml
----

In the end you should have output like this:
[source, text]
----
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 172.31.114.133:6443 --token cpj9ge.fzrh8x2qunkpp9pf \
	--discovery-token-ca-cert-hash sha256:3b24ae8de2851aedd2370db06407ea8023c208799e8a4811589453237bb7a2a7
----

for now just do this instruction:

[source,bash]
----
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
----

=== Installing a Pod network add-on

You must deploy a Container Network Interface (CNI) based Pod network add-on so that your Pods can communicate with each other. Cluster DNS (CoreDNS) will not start up before a network is installed.

==== Option 1: Flannel network

Now we need to initialize Flannel network in it:

[source,bash]
----
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
----


==== Option 2: Calico network

[source,bash]
----
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
----

=== Join your nodes to the master

Please use `kubeadm join ... ` command you had in the kubeadm output.

You can access it with this command:

[source,bash]
----
kubeadm token create --print-join-command
----


=== Install Prometheus

Add the latest helm repository in Kubernetes

[source,bash]
----
helm repo add stable https://charts.helm.sh/stable
----

Add the Prometheus community helm chart in Kubernetes

[source,bash]
----
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
----

[source,bash]
----
helm repo update
----

once added search Prometheus community helm chart to install

[source,bash]
----
helm search repo prometheus-community
----

You will see available Prometheus and grafana helm chart to install

Prometheus and grafana helm chart moved to kube prometheus stack

Below is helm command to install kube-prometheus-stack

[source,text]
----
helm install [RELEASE_NAME] prometheus-community/kube-prometheus-stack
----

Lets install stable prometheus-community/kube-prometheus-stack

[source,bash]
----
helm install monitoring prometheus-community/kube-prometheus-stack
----

once installed, you will see below output

Output:

[source,text]
----
NAME: monitoring
LAST DEPLOYED: Thu Sep 29 21:27:28 2022
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
kube-prometheus-stack has been installed. Check its status by running:
  kubectl --namespace default get pods -l "release=monitoring"

Visit https://github.com/prometheus-operator/kube-prometheus for instructions on how to create & configure Alertmanager and Prometheus instances using the Operator.
----

Sync rest with https://www.fosstechnix.com/install-prometheus-and-grafana-on-kubernetes-using-helm/

=== Metrics

==== Option 1: Metrics Server

https://artifacthub.io/packages/helm/metrics-server/metrics-server

[source,bash]
----
helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
----

[source,bash]
----
helm upgrade --install metrics-server metrics-server/metrics-server
----

Not working at the moment - needs to do steps described here: https://particule.io/en/blog/kubeadm-metrics-server/ and write about them upper


==== Option 2: Prometheus metrics

File: `metrics-value.yml`

[source,yaml]
----
prometheus:
  url: http://stable-kube-prometheus-sta-prometheus

rules:
  resource:
    cpu:
      containerQuery: sum(rate(container_cpu_usage_seconds_total{<<.LabelMatchers>>, container!=""}[3m])) by (<<.GroupBy>>)
      nodeQuery: sum(rate(container_cpu_usage_seconds_total{<<.LabelMatchers>>, id='/'}[3m])) by (<<.GroupBy>>)
      resources:
        overrides:
          node:
            resource: node
          namespace:
            resource: namespace
          pod:
            resource: pod
      containerLabel: container
    memory:
      containerQuery: sum(container_memory_working_set_bytes{<<.LabelMatchers>>, container!=""}) by (<<.GroupBy>>)
      nodeQuery: sum(container_memory_working_set_bytes{<<.LabelMatchers>>,id='/'}) by (<<.GroupBy>>)
      resources:
        overrides:
          node:
            resource: node
          namespace:
            resource: namespace
          pod:
            resource: pod
      containerLabel: container
    window: 3m
----

Not all working with this option as `kubectl top node` not showing metrics

[source,bash]
----
helm upgrade -i metrics prometheus-community/prometheus-adapter -f metrics-value.yml
----

=== Storage class

localdisk-sc.yml

[source,yaml]
----
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: localdisk
provisioner: kubernetes.io/no-provisioner
allowVolumeExpansion: true
volumeBindingMode: WaitForFirstConsumer
----

kubectl patch storageclass localdisk -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'


localdisk-pv-1.yml

[source,yaml]
----
kind: PersistentVolume
apiVersion: v1
metadata:
  name: localdisk-pv-1
spec:
  storageClassName: localdisk
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: /var/data
----


[source,bash]
----
sudo mkdir /var/data
sudo chmod 777 /var/data
----

https://metallb.universe.tf/installation/
https://kubernetes.github.io/ingress-nginx/deploy/#quick-start
https://kubernetes.github.io/ingress-nginx/deploy/baremetal/

First, use Helm to install the nginx Ingress controller.
helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update
kubectl create namespace nginx-ingress
helm install nginx-ingress nginx-stable/nginx-ingress -n nginx-ingress
Get the cluster IP of the Ingress controller's Service.
kubectl get svc nginx-ingress-nginx-ingress -n nginx-ingress

TODO: Cluster IP for now - read how to have external ip

https://github.com/kklis/proxy


https://traefik.io/blog/install-and-configure-traefik-with-helm/

[source,yaml]
----
---
additionalArguments:
  - --entrypoints.websecure.http.tls.certresolver=cloudflare
  - --entrypoints.websecure.http.tls.domains[0].main=thunderwolf.net
  - --entrypoints.websecure.http.tls.domains[0].sans=*.thunderwolf.net
  - --certificatesresolvers.cloudflare.acme.dnschallenge.provider=cloudflare
  - --certificatesresolvers.cloudflare.acme.email=atulodzi@gmail.com
  - --certificatesresolvers.cloudflare.acme.dnschallenge.resolvers=1.1.1.1
  - --certificatesresolvers.cloudflare.acme.storage=/certs/acme.json

ports:
  web:
    redirectTo: websecure

env:
  - name: CF_API_EMAIL
    valueFrom:
      secretKeyRef:
        key: email
        name: cloudflare-api-credentials
  - name: CF_API_KEY
    valueFrom:
      secretKeyRef:
        key: apiKey
        name: cloudflare-api-credentials

ingressRoute:
  dashboard:
    enabled: false

persistence:
  enabled: true
  path: /certs
  size: 128Mi
  storageClass: localdisk
----

=== OTHER

On the Node

[source,bash]
----
sudo mkdir /mnt/data
----

[source,yaml]
----
apiVersion: v1
kind: PersistentVolume
metadata:
  name: task-pv-volume
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data"
----


image:https://i.creativecommons.org/l/by/4.0/88x31.png[Creative Commons Licence,88,31] This work is licensed under a http://creativecommons.org/licenses/by/4.0/[Creative Commons Attribution 4.0 International License]
